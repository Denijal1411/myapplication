﻿using BussinessLogicCR.Models;
using DAL;
using DAL.InterfaceRepository;
using DAL.Models; 
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicCR
{
    public class EnglishWordBussinessLogic : IEnglishWordBussinessLogic
    {
        //private readonly IRepository<EnglishWord> englishWord; 
        private readonly IUnitOfWork _unitOfWork;
        public EnglishWordBussinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public EnglishWordBussinessLogic()
        {

        }

        public List<EnglishWordModel> GetAll()
        {
            List<EnglishWordModel> listItems = new List<EnglishWordModel>();
            foreach (var item in _unitOfWork.EnglishWord.GetAll())
            {
                listItems.Add(ConvertFromDBModel(item));
            }
            return listItems;
        }


        public EnglishWordModel GetById(int id)
        {
            return ConvertFromDBModel(_unitOfWork.EnglishWord.GetById(id));
        }


        public EnglishWord ConvertToDBModel(EnglishWordModel dBModel)
        {
            return new EnglishWord()
            {
                 Description=dBModel.Description,
                 Id=dBModel.Id,
                 PictureName=dBModel.PictureName,
                 Status=dBModel.Status,
                 Word=dBModel.Word
            };
        }

        public EnglishWordModel ConvertFromDBModel(EnglishWord model)
        {
            return new EnglishWordModel()
            {
                Description = model.Description,
                Id = model.Id,
                PictureName = model.PictureName,
                Status = model.Status,
                Word = model.Word
            };
        }
        private async Task GetApiDataAsync()
        {
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri("https://google-translate1.p.rapidapi.com/language/translate/v2/detect"),
                Headers =
    {
        { "x-rapidapi-host", "google-translate1.p.rapidapi.com" },
        { "x-rapidapi-key", "79c81d35cemshe329a62fee04088p1d38cbjsn9b944194d368" },
    },
                Content = new FormUrlEncodedContent(new Dictionary<string, string>
    {
        { "q", "English is hard, but detectably so" },
    }),
            };
            using (var response = await client.SendAsync(request))
            {
                //response.EnsureSuccessStatusCode();
                var body = await response.Content.ReadAsStringAsync();
                Console.WriteLine(body);
            }
        }

        public void Create(EnglishWordModel model)
        {
            
            _unitOfWork.EnglishWord.Insert(ConvertToDBModel(model));
            _unitOfWork.Save();
        }

        public void Update(EnglishWordModel model)
        {
            GetApiDataAsync();
            var word = _unitOfWork.EnglishWord.GetById(model.Id); 
            word.Word = model.Word;
            word.Description = model.Description;
            word.PictureName = model.PictureName;
            _unitOfWork.Save();

             
        }


        public void Delete(int id)
        {
            _unitOfWork.EnglishWord.Delete(id);
           _unitOfWork.Save();
        }
    }
}
