﻿using BussinessLogicCR.Models;
using DAL.Models; 
using System;
using System.Collections.Generic;
using System.Text;

namespace BussinessLogicCR
{
    public interface IEnglishWordBussinessLogic
    {
        List<EnglishWordModel> GetAll();
        void Create(EnglishWordModel model);
        void Delete(int id);
        void Update(EnglishWordModel model);
        EnglishWordModel GetById(int id);
        EnglishWord ConvertToDBModel(EnglishWordModel dBModel);
    }
}
