﻿using BussinessLogicCR.Models;
using DAL.InterfaceRepository;
using DAL.Models; 
using System;
using System.Collections.Generic;
using System.Text;

namespace BussinessLogicCR
{
    public interface IToDoBussinessLogic
    {
        List<DoItemModel> GetAll();
        void Create(string text);
        void ChangeStatusToActivate(int id);
        void Delete(int id);

        DoItemModel GetById(int id); 
    }
}
