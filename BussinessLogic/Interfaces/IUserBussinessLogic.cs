﻿using BussinessLogicCR.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BussinessLogicCR
{
    public interface IUserBussinessLogic
    {
        List<UserModel> GetAll();
        void Create(UserModel model);
        void Delete(int id);

        UserModel GetById(int id);
    }
}
