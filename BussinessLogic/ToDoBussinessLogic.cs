﻿using BussinessLogicCR.Models;
using DAL;
using DAL.DoItemInterface;
using DAL.EnglishWordInterface;
using DAL.InterfaceRepository;
using DAL.Models; 
using System; 
using System.Collections.Generic;
using System.Linq;

namespace BussinessLogicCR
{
    public class ToDoBussinessLogic:IToDoBussinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

         
        public ToDoBussinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ToDoBussinessLogic()
        {

        }

        public List<DoItemModel> GetAll()
        {
            List<DoItemModel> listItems = new List<DoItemModel>();
            foreach (var item in _unitOfWork.DoItem.GetAll().OrderByDescending(x => x.Active).ThenByDescending(x => x.DateAdded))
            {
                listItems.Add(ConvertFromDBModel(item));
            }
            return listItems;
        }
        

        public DoItemModel GetById(int id)
        {
            return ConvertFromDBModel(_unitOfWork.DoItem.GetById(id)); 
        }
         

        private DoItem ConvertToDBModel(DoItemModel dBModel)
        {
            return new DoItem()
            {
                Active = dBModel.Active,
                DateAdded = dBModel.DateAdded,
                Id = dBModel.Id,
                Item = dBModel.Item
            };
        }

        public DoItemModel ConvertFromDBModel(DoItem model)
        {
            return new DoItemModel()
            {
                Item = model.Item,
                Id = model.Id,
                DateAdded = model.DateAdded,
                Active = model.Active

            };
        }
          

        public void ChangeStatusToActivate(int id)
        {
            var item = _unitOfWork.DoItem.GetById(id);
            item.Active = false;
            _unitOfWork.Save();
             
        }
         

        public void Delete(int id)
        {
            _unitOfWork.DoItem.Delete(id);
            _unitOfWork.Save();
        }


        public void Create(string text)
        {
            var model = new DoItemModel()
            {
                Item = text,
                Active = true,
                DateAdded = DateTime.Now

            };

            _unitOfWork.DoItem.Insert(ConvertToDBModel(model));
            _unitOfWork.Save();
        }
         
    }
}
