﻿using BussinessLogicCR.Models;
using DAL.InterfaceRepository;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BussinessLogicCR
{
    public class UserBussinessLogic : IUserBussinessLogic
    {
        private readonly IUnitOfWork _unitOfWork; 
        public UserBussinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void Create(UserModel model)
        {
            _unitOfWork.User.Insert(ConvertToDBModel(model));
            _unitOfWork.Save();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<UserModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public UserModel GetById(int id)
        {
            throw new NotImplementedException();
        }
        private User ConvertToDBModel(UserModel dBModel)
        {
            return new User()
            { 
                 Password=dBModel.Password,
                 Username=dBModel.Username

            };
        }
    }
}
