﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BussinessLogicCR.Models
{
    public class DoItemModel
    { 
        public int Id { get; set; }
        public bool Active { get; set; }
        [Required]
        [StringLength(3000)]
        public string Item { get; set; } 
        public DateTime? DateAdded { get; set; }
    }
}
