﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BussinessLogicCR.Models
{
    public class EnglishWordModel
    { 
        public int Id { get; set; }
        [Required]
        [StringLength(2000)]
        public string Word { get; set; }
        [Required]
        [StringLength(2000)]
        public string Description { get; set; }
        [StringLength(1000)]
        public string PictureName { get; set; }
        public int Status { get; set; }
    }
}
