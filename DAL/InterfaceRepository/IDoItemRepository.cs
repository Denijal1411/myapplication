﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Models;

namespace DAL.DoItemInterface
{
    public interface IDoItemRepository :IRepository<DoItem>
    {
    }
}
