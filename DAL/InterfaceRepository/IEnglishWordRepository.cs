﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.EnglishWordInterface
{
    public interface IEnglishWordRepository:IRepository<EnglishWord>
    {
    }
}
