﻿using DAL.DoItemInterface;
using DAL.EnglishWordInterface;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.InterfaceRepository
{
    public interface IUnitOfWork:IDisposable
    {
        IEnglishWordRepository EnglishWord { get; }
        IDoItemRepository DoItem { get; }
        IUserRepository User { get; }
        void Save();
    }
}
