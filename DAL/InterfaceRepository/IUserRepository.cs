﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.InterfaceRepository
{
    public interface IUserRepository:IRepository<User>
    {
    }
}
