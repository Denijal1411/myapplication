﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace DAL.Models
{
    public partial class DoItem
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        public bool Active { get; set; }
        [Required]
        [StringLength(3000)]
        public string Item { get; set; }
        [Column(TypeName = "date")]
        public DateTime? DateAdded { get; set; }
    }
}
