﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace DAL.Models
{
    [Table("EnglishWord")]
    public partial class EnglishWord
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Required]
        [StringLength(2000)]
        public string Word { get; set; }
        [Required]
        [StringLength(2000)]
        public string Description { get; set; }
        [StringLength(1000)]
        public string PictureName { get; set; }
        public int Status { get; set; }
    }
}
