﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.DoItemInterface
{
    public class DoItemRepository : Repository<DoItem>, IDoItemRepository
    {
        private readonly Contexts _dbcontext;
        public DoItemRepository(Contexts dbcontext) : base(dbcontext)
        {
            _dbcontext = dbcontext;
        }
    }
}
