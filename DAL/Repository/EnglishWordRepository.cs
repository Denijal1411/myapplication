﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.EnglishWordInterface
{
    public class EnglishWordRepository :Repository<EnglishWord>,IEnglishWordRepository
    {
        private readonly Contexts _dbcontext;
        public EnglishWordRepository(Contexts dbcontext):base(dbcontext)
        {
            _dbcontext = dbcontext;
        }
    }
}
