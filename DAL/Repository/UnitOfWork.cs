﻿using DAL.DoItemInterface;
using DAL.EnglishWordInterface;
using DAL.InterfaceRepository;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        public IEnglishWordRepository EnglishWord { get; private set; }
        public IDoItemRepository DoItem { get; private set; } 
        public IUserRepository User { get; private set; }

        private readonly Contexts _dbcontext;
        public UnitOfWork(Contexts dbcontext)
        {
            _dbcontext = dbcontext;
            EnglishWord = new EnglishWordRepository(_dbcontext);
            DoItem = new DoItemRepository(_dbcontext);
        }
         

        public void Dispose()
        {
            _dbcontext.Dispose();
        }

        public void Save()
        {
            _dbcontext.SaveChanges();
        }
    }
}
