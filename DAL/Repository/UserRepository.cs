﻿using DAL.InterfaceRepository;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private readonly Contexts _dbcontext;
        public UserRepository(Contexts dbcontext) : base(dbcontext)
        {
            _dbcontext = dbcontext;
        }
    }
}
