﻿using BussinessLogicCR;
using BussinessLogicCR.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace MyApplication.Controllers
{
    public class DoItemController : Controller
    {
        private readonly IToDoBussinessLogic _bussinesslogic;
        public DoItemController(IToDoBussinessLogic bussinesslogic)
        {
            _bussinesslogic = bussinesslogic;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult ToDoApp(int? page)
        {
            var a = _bussinesslogic.GetAll().ToList();
            int pageSize = 16;
            int pageNumber = (page ?? 1);
            return View(a.ToPagedList(pageNumber, pageSize));
        }
        public IActionResult AddtoDoItem(string text)
        {  

            _bussinesslogic.Create(text.Trim());

            return RedirectToAction("ToDoApp", "DoItem");
        }
        public IActionResult Done(int id)
        {
            _bussinesslogic.ChangeStatusToActivate(id); 
            return RedirectToAction("ToDoApp", "DoItem");
        }
        public IActionResult Delete(int id)
        { 
            _bussinesslogic.Delete(id); 
            return RedirectToAction("ToDoApp", "DoItem");
        }
        public IActionResult Privacy()
        {
            return View();
        }
    }
}
