﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using BussinessLogicCR;
using BussinessLogicCR.Models;
using DAL.InterfaceRepository;
using DAL.Models;
using DinkToPdf;
using DinkToPdf.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc; 

namespace MyApplication.Controllers
{
    public class EnglishWordController : Controller
    {
        private readonly IEnglishWordBussinessLogic _bussinesslogic;
        public EnglishWordController(IEnglishWordBussinessLogic bussinesslogic)
        {
            _bussinesslogic = bussinesslogic;
        }
        public IActionResult Index()
        {  
            return View();
        }
        public IActionResult AddEnglishWord()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult AddEnglishWord(EnglishWordModel  model)
        { 

            if (ModelState.IsValid) {
                _bussinesslogic.Create(model); 
                ViewBag.msg = "Success!";
            }

            return View();

        }  
        public IActionResult ExerSizePage() {
            

            return View();
        }
        public IActionResult ExerSizePageOneWord() {
            return View();
        }
        public IActionResult ShowAllWords() {
            var words = _bussinesslogic.GetAll(); 
            return View(words);
        }
        [HttpPost]
        public IActionResult FindWord(string lan)
        {
            if (lan == null) lan = "English";
            ViewBag.Language = lan;
            var word = _bussinesslogic.GetAll().Where(x => x.Status == 2 || x.Status == 3).OrderBy(x => Guid.NewGuid()).Take(10).ToList();
            return View(word);
        }
        [HttpPost]
        public IActionResult ShowNumberWords(int number,int status)
        {
            var words = _bussinesslogic.GetAll().Where(x=>x.Status==status).OrderBy(x => Guid.NewGuid()).Take(number).ToList();  
             return View(words);
        }
        public IActionResult EditWord(int id)
        {
            var word = _bussinesslogic.GetAll().FirstOrDefault(x => x.Id == id);
            return View(word);
        }
        public IActionResult DeleteWord(int id)
        {
            var word = _bussinesslogic.GetById(id).Id;
            _bussinesslogic.Delete(word); 

            return RedirectToAction("ShowAllWords", "EnglishWord");
        }
        public IActionResult WordWithPicture()
        {


            return View();
        }
        [HttpPost]
        public IActionResult WordWithPicture(int status)
        {
            var words = _bussinesslogic.GetAll().Where(x => x.Status == status).OrderBy(x => Guid.NewGuid()).ToList();
            return View(words);
        } 
        public IActionResult _PartialWord(int id)
        {
            var data = _bussinesslogic.GetById(id);
            return PartialView("_PartialWord", data);
        }
        [HttpPost]
        public IActionResult ChangeWord(EnglishWordModel model) { 
            _bussinesslogic.Update(model);
            return RedirectToAction("ShowAllWords", "EnglishWord");
        }
        
        public  IActionResult PrintPdf()
        {
            string ApiUrl = "http://localhost:8022/";
             
            var responseString = "";
            var request = (HttpWebRequest)WebRequest.Create(ApiUrl);
            request.Method = "GET";
            request.ContentType = "application/json";

            using (var response1 = request.GetResponse())
            {
                using (var reader = new StreamReader(response1.GetResponseStream()))
                {
                    responseString = reader.ReadToEnd();
                }
            }
            return RedirectToAction("ShowAllWords", "EnglishWord");

        }

    }
}
