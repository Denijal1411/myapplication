﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BussinessLogicCR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BussinessLogicCR.Models;
using X.PagedList;
using DAL.InterfaceRepository;

namespace MyApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly IToDoBussinessLogic _bussinesslogic;
        private readonly IUserBussinessLogic _bussinesslogicuser;
        public HomeController(IToDoBussinessLogic bussinesslogic, IUserBussinessLogic bussinesslogicuser)
        {
            _bussinesslogic = bussinesslogic;
            _bussinesslogicuser = bussinesslogicuser;
        }


        public IActionResult Index()
        {
            return View();
        }
       

        public IActionResult Login()
        {
            return View();
        }
       [HttpPost]
        public IActionResult Registration(UserModel model)
        {
            if (ModelState.IsValid)
            {
                _bussinesslogicuser.Create(model);
            }
            return View();
        }
       

        public IActionResult Registration()
        {
            return View();
        }
       

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View() ;
        }
    }
}
