﻿using BussinessLogicCR;
using DAL;
using DAL.DoItemInterface;
using DAL.EnglishWordInterface;
using DAL.InterfaceRepository;
using DAL.Models;
using DAL.Repository;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace MyApplication
{
    public static class ServiceExtension
    {
        public static void ConfigureRepositoryUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>(); 
            services.AddScoped<IEnglishWordRepository, EnglishWordRepository>(); 
            services.AddScoped<IUserRepository, UserRepository>(); 
            services.AddScoped<IDoItemRepository, DoItemRepository>(); 
            services.AddScoped<IToDoBussinessLogic, ToDoBussinessLogic>(); 
            services.AddScoped<IEnglishWordBussinessLogic, EnglishWordBussinessLogic>(); 
            services.AddScoped<IUserBussinessLogic, UserBussinessLogic>(); 
            services.AddScoped<Contexts>(); 
        }
    }
}
