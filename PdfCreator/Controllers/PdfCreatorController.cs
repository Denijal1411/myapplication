﻿using DinkToPdf;
using DinkToPdf.Contracts;
using Microsoft.AspNetCore.Mvc;
using PdfCreator.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PdfCreator.Controllers
{
     
    [Route("api/pdfcreator")]
    [Route("")]
    [ApiController]
    public class PdfCreatorController : Controller
    {
        private IConverter _converter;
        public PdfCreatorController(IConverter converter)
        {
            _converter = converter;
        }
       
        [HttpGet]
        public IActionResult CreatePDF()
        {
            string wwwPath = @"D:\pdfcreator";
            string path = Path.Combine(wwwPath);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            
            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10 },
                DocumentTitle = "Nepoznate Reci",
                Out = @"D:\PDFCreator\English_"+DateTime.Now.ToString("dd.MM.yyyy.")+".pdf"
            };
            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = Searcher.GetHTMLString(),
                WebSettings = { DefaultEncoding = "utf-8"
        },
                HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "Page [page] of [toPage]", Line = true },
                FooterSettings = { FontName = "Arial", FontSize = 9, Line = true, Center = DateTime.Now.ToString("dd.MM.yyyy.") }
            };
            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };
            _converter.Convert(pdf);
            return Ok("Successfully created PDF document.");
        } 
}
}
