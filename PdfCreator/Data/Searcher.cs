﻿using MyApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdfCreator.Data
{
    public class Searcher
    { 
        static Contexts  Contexts = new Contexts();
        public static string GetHTMLString()
        {
            var employees = Contexts.EnglishWords.Where(x => x.Status == 2 || x.Status == 3).OrderBy(x => Guid.NewGuid()).ToList();
            var sb = new StringBuilder();
            sb.Append(@"
                        <html>
                            <head> 
                            </head>
                            <body>
                                <style>
                                    .status2{background-color:#e58098;}
                                    .status3{background-color:#58c270;}
                                </style>
                                <div class='header'><h1>Nepoznate Reci</h1></div>
                                <table align='center'>
                                    <tr>
                                        <th>Word</th>
                                        <th>Description</th> 
                                    </tr>");
            foreach (var emp in employees)
            {
                sb.AppendFormat(@"<tr class="+(emp.Status==2? "status2": "status3" )+ @">
                                    <td>{0}</td>
                                    <td>{1}</td> 
                                  </tr>", emp.Word, emp.Description);
            }
            sb.Append(@"
                                </table>
                            </body>
                        </html>");
            return sb.ToString();
        }
    }
}
